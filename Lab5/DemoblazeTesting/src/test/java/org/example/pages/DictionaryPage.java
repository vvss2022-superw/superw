package org.example.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.junit.Assert;

@DefaultUrl("https://www.demoblaze.com")
public class DictionaryPage extends PageObject {

    private String product_name_website="Iphone 6 32gb";

    @FindBy(id="loginusername")
    private WebElementFacade userInput;

    @FindBy(id="loginpassword")
    private WebElementFacade passInput;

    @FindBy(id = "login2")
    private WebElementFacade loginButton;

    @FindBy(xpath = "//*[@id=\"logInModal\"]/div/div/div[3]/button[2]")
    private WebElementFacade login;

    @FindBy(id = "logout2")
    private WebElementFacade logoutButton;

    @FindBy(xpath ="//*[@id=\"tbodyid\"]/div[5]/div/a/img")
    private WebElementFacade product;

    @FindBy(xpath = "//*[@id=\"tbodyid\"]/h2")
    private WebElementFacade product_name;

    @FindBy(xpath = "//*[@id=\"tbodyid\"]/div[2]/div/a")
    private WebElementFacade addButton;

    @FindBy(xpath = "//*[@id=\"tbodyid\"]/tr/td[4]/a")
    private WebElementFacade deleteButton;

    @FindBy(xpath = "//*[@id=\"tbodyid\"]/tr/td[2]")
    private WebElementFacade product_name_cart;

    @FindBy(id="cartur")
    private WebElementFacade cartButton;


    public void enter_user(String keyword) {
        userInput.type(keyword);
    }

    public void enter_pass(String keyword) {
        passInput.type(keyword);
    }

    public void press_login_button()
    {
        loginButton.click();
    }

    public void press_login_final()
    {
        login.click();
    }


    public void check_logout()
    {
        Assert.assertTrue(logoutButton.isClickable());
    }

    public void go_product()
    {

        product.click();
    }

    public void check_product_name()
    {
        if(product_name.getText().equals(product_name_website))
            Assert.assertTrue(true);
        else
            Assert.assertTrue(false);
    }

    public void add_product()
    {
        addButton.click();
    }

    public void press_cart_button()
    {
        cartButton.click();
    }

    public void delete_product()
    {
        deleteButton.click();
    }

    public void press_logout_button()
    {
        logoutButton.click();
        Assert.assertTrue(loginButton.isClickable());
    }

    public void check_product() {
        if(product_name_cart.getText().equals(product_name_website))
            Assert.assertTrue(true);
        else
            Assert.assertTrue(false);
    }
}