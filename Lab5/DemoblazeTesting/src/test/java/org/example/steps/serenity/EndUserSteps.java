package org.example.steps.serenity;

import net.thucydides.core.annotations.Step;
import org.example.pages.DictionaryPage;
import org.junit.Assert;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class EndUserSteps {

    DictionaryPage dictionaryPage;

    @Step
    public void is_login_page()
    {
        dictionaryPage.open();
    }


    @Step
    public void email(WebDriver webDriver,String username)
    {
        //WebDriverWait wait = new WebDriverWait(webDriver, 5);
        //wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("loginusername")));
        dictionaryPage.enter_user(username);
    }

    @Step
    public void pass(String password)
    {
        dictionaryPage.enter_pass(password);
    }


    @Step
    public void pass_failed(String password_fail)
    {
        dictionaryPage.enter_pass(password_fail);
    }

    @Step
    public void login(WebDriver webDriver)
    {
        //WebDriverWait wait = new WebDriverWait(webDriver, 5);
        //wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("login2")));
        dictionaryPage.press_login_button();
    }

    @Step
    public void login_end()
    {
        dictionaryPage.press_login_final();
    }

    @Step
    public void is_logout_possible(WebDriver webDriver)
    {
        WebDriverWait wait = new WebDriverWait(webDriver, 5);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("logout2")));
        dictionaryPage.check_logout();
    }

    @Step
    public void login_failed_alert(WebDriver webDriver)
    {

        WebDriverWait wait = new WebDriverWait(webDriver, 10 /*timeout in seconds*/);
        if(wait.until(ExpectedConditions.alertIsPresent())==null) {
            //System.out.println("alert was not present");
            Assert.assertTrue(false);
        }
        else {
            //System.out.println("alert was present");
            Alert alert=webDriver.switchTo().alert();
            alert.accept();
            Assert.assertTrue(true);
        }
    }

    @Step
    public void go_to_product()
    {
        dictionaryPage.go_product();
    }



    @Step
    public void check_product_name()
    {
        dictionaryPage.check_product_name();
    }

    @Step
    public void add_product_to_cart(WebDriver webDriver)
    {
        dictionaryPage.add_product();
        WebDriverWait wait = new WebDriverWait(webDriver, 10 /*timeout in seconds*/);
        if(wait.until(ExpectedConditions.alertIsPresent())==null) {
            //System.out.println("alert was not present");
            Assert.assertTrue(false);
        }
        else {
            //System.out.println("alert was present");
            Assert.assertTrue(true);
        }
    }


    @Step
    public void go_to_cart()
    {
        dictionaryPage.press_cart_button();
    }

    @Step
    public void check_product(){
        dictionaryPage.check_product();
    }

    @Step
    public void delete_product_from_cart(WebDriver webDriver)
    {
        dictionaryPage.delete_product();
        if(webDriver.findElements( By.xpath("//*[@id=\"tbodyid\"]/tr/td[4]/a")).size() != 0)
            Assert.assertTrue(true);
        else {
            Assert.assertTrue(false);
        }
    }

    public void logout(WebDriver webdriver)
    {
        dictionaryPage.press_logout_button();
    }
}