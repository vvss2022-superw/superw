package org.example.features.search;

import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.junit.annotations.UseTestDataFrom;
import org.example.steps.serenity.EndUserSteps;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.openqa.selenium.WebDriver;

@RunWith(SerenityParameterizedRunner.class)
@UseTestDataFrom(value="src/test/resources/features/search/config.csv")
public class SearchByKeywordStory{

    private String username;
    private String password;
    private String password2;


    @Managed(uniqueSession = true, driver="chrome")
    public WebDriver webdriver;

    @Steps
    public EndUserSteps superw;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword2() {
        return password2;
    }

    public void setPassword_fail(String password_fail) {
        this.password2 = password_fail;
    }

    @Test
    public void login_passed()
    {
        System.out.println(username);
        superw.is_login_page();
        superw.login(webdriver);
        superw.email(webdriver,username);
        superw.pass(password);
        superw.login_end();
        superw.is_logout_possible(webdriver);
        superw.logout(webdriver);
    }

    @Test
    public void login_failed()
    {
        System.out.println(username);
        superw.is_login_page();
        superw.login(webdriver);
        superw.email(webdriver,username);
        superw.pass_failed(password2);
        superw.login_end();
        superw.login_failed_alert(webdriver);
    }

    @Test
    public void scenario_test()
    {
        System.out.println(username);
        //login
        superw.is_login_page();
        superw.login(webdriver);
        superw.email(webdriver,username);
        superw.pass(password);
        superw.login_end();
        superw.is_logout_possible(webdriver);
        //login end

        //add_product
        superw.go_to_product();
        superw.check_product_name();
        superw.add_product_to_cart(webdriver);
        //add_product_end


        //check & delete_product
        superw.go_to_cart();
        superw.check_product();
        superw.delete_product_from_cart(webdriver);
        //delete_product_end

        //logout
        superw.logout(webdriver);
        //logout_end
    }
} 