package inventory.service;

import inventory.model.InhousePart;
import inventory.model.Part;
import inventory.model.Product;
import inventory.repository.InventoryRepository;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ServiceIntegrationTest {
    private static final String SEARCH = "dulap";
    private static final String SEARCH_PART = "lemn";

    private static InventoryService inventoryService = new InventoryService(new InventoryRepository());

    private static Part part;

    private static Product product;

    @BeforeAll
    static void setUp() {
        part = new InhousePart(1, "lemn", 1, 5, 3, 10, 1);
        ObservableList<Part> productParts = FXCollections.observableArrayList();
        productParts.add(part);

        product = new Product(1, "dulap", 10.0, 10, 5, 15, productParts);
        inventoryService.addInhousePart("lemn", 1, 5, 3, 10, 1);
        inventoryService.addProduct("dulap", 10.0, 10, 5, 15, productParts);
    }

    @Test
    void lookupProductTest() {

        Product result = inventoryService.lookupProduct(SEARCH);

        assertEquals(product.getName(), result.getName());
        assertEquals(product.getPrice(), result.getPrice());
        assertEquals(product.getInStock(), result.getInStock());
    }

    @Test
    void lookupPartTest() {

        Part result = inventoryService.lookupPart(SEARCH_PART);

        assertEquals(part.getName(), result.getName());
        assertEquals(part.getPrice(), result.getPrice());
        assertEquals(part.getInStock(), result.getInStock());
    }
}
