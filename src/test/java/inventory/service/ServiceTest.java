package inventory.service;

import inventory.model.InhousePart;
import inventory.model.Part;
import inventory.model.Product;
import inventory.repository.InventoryRepository;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ServiceTest {

    @InjectMocks
    private InventoryService service;

    @Mock
    private InventoryRepository repository;

    private static ObservableList<Part> productParts;

    private static ObservableList<Product> products;

    @BeforeAll
    static void setUp() {
        productParts = FXCollections.observableArrayList();
        productParts.add(new InhousePart(1, "lemn", 1, 5, 3, 10, 1));

        products = FXCollections.observableArrayList();
        Product product = new Product(1, "dulap", 10.0, 10, 5, 15, productParts);
        products.add(product);
    }

    @Test
    void getAllProductsTest() {
        when(repository.getAllProducts()).thenReturn(products);

        ObservableList<Product> result = service.getAllProducts();

        assertEquals(products, result);
        verify(repository).getAllProducts();
    }

    @Test
    void getAllPartsTest() {
        when(repository.getAllParts()).thenReturn(productParts);

        ObservableList<Part> result = service.getAllParts();

        assertEquals(productParts, result);
        verify(repository).getAllParts();
    }
}
