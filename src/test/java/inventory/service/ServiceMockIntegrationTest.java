package inventory.service;

import inventory.model.InhousePart;
import inventory.model.Inventory;
import inventory.model.Part;
import inventory.model.Product;
import inventory.repository.InventoryRepository;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ServiceMockIntegrationTest {

    private static final String SEARCH = "dulap";
    private static final String SEARCH_PART = "lemn";

    @InjectMocks
    private static InventoryRepository repository;

    @Mock
    private static Inventory inventory;

    private static InventoryService inventoryService;

    private static Part part;

    private static Product product;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        inventoryService = new InventoryService(repository);

        part = new InhousePart(1, "lemn", 1, 5, 3, 10, 1);
        ObservableList<Part> productParts = FXCollections.observableArrayList();
        productParts.add(part);

        product = new Product(1, "dulap", 10.0, 10, 5, 15, productParts);
    }

    @Test
    void lookupProductTest() {
        when(inventory.lookupProduct(SEARCH)).thenReturn(product);

        Product result = inventoryService.lookupProduct(SEARCH);

        assertEquals(product, result);
        verify(inventory).lookupProduct(SEARCH);
    }

    @Test
    void lookupPartTest() {
        when(inventory.lookupPart(SEARCH_PART)).thenReturn(part);

        Part result = inventoryService.lookupPart(SEARCH_PART);

        assertEquals(part, result);
        verify(inventory).lookupPart(SEARCH_PART);
    }
}
