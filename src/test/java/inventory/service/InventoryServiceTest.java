package inventory.service;

import inventory.model.InhousePart;
import inventory.model.Part;
import inventory.repository.InventoryRepository;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@TestMethodOrder(MethodOrderer.Random.class)
class InventoryServiceTest {

    private static final String PRODUCT_NAME = "Laptop";
    private static final double PRODUCT_PRICE = 5000;
    private static ObservableList<Part> productParts;

    private final InventoryService inventoryService = new InventoryService(new InventoryRepository());

    @BeforeAll
    static void setUp() {
        productParts = FXCollections.observableArrayList();
        productParts.add(new InhousePart(1, "cable", 1, 5, 3, 10, 1));
    }

    @DisplayName("ProductAdded")
    @Test
    void testAddProductSuccess() {
        int expectedListSize = inventoryService.getAllProducts().size() + 1;

        inventoryService.addProduct(PRODUCT_NAME, PRODUCT_PRICE, 2, 1, 15, productParts);
        int actualListSize = inventoryService.getAllProducts().size();

        assertEquals(expectedListSize, actualListSize);
    }

    @RepeatedTest(value = 5, name = "{displayName} {currentRepetition}/{totalRepetitions}")
    @DisplayName("StockSmallerThanMin")
    void testAddProductStockSmallerThanMin() {
        String expectedErrorMessage = "Inventory level is lower than minimum value. ";

        Throwable exception = assertThrows(RuntimeException.class,
                () -> inventoryService.addProduct(PRODUCT_NAME, PRODUCT_PRICE, 0, 1, 15, productParts));

        assertEquals(expectedErrorMessage, exception.getMessage());
    }

    @Test
    @DisplayName("StockBiggerThanMax")
    void testAddProductStockBiggerThanMax() {
        String expectedErrorMessage = "Inventory level is higher than the maximum value. ";

        Throwable exception = assertThrows(RuntimeException.class,
                () -> inventoryService.addProduct(PRODUCT_NAME, PRODUCT_PRICE, 16, 1, 15, productParts));

        assertEquals(expectedErrorMessage, exception.getMessage());
    }

    @Test
    @DisplayName("MinIsNegative")
    void testAddProductMinNegative() {
        String expectedErrorMessage = "The inventory level must be greater than 0. ";

        Throwable exception = assertThrows(RuntimeException.class,
                () -> inventoryService.addProduct(PRODUCT_NAME, PRODUCT_PRICE, 1, -1, 15, productParts));

        assertEquals(expectedErrorMessage, exception.getMessage());
    }

    @Test
    @DisplayName("StockEqualToMin")
    void testAddProductStockEqualMin() {
        int expectedListSize = inventoryService.getAllProducts().size() + 1;

        inventoryService.addProduct(PRODUCT_NAME, PRODUCT_PRICE, 1, 1, 15, productParts);
        int actualListSize = inventoryService.getAllProducts().size();

        assertEquals(expectedListSize, actualListSize);
    }

    @Test
    @DisplayName("StockIsNegative")
    void testAddProductStockNegative() {
        String expectedErrorMessage = "Inventory level is lower than minimum value. ";

        Throwable exception = assertThrows(RuntimeException.class,
                () -> inventoryService.addProduct(PRODUCT_NAME, PRODUCT_PRICE, -1, 1, 15, productParts));

        assertEquals(expectedErrorMessage, exception.getMessage());
    }

    @Test
    @DisplayName("StockEqualToMax")
    void testAddProductStockEqualMax() {
        int expectedListSize = inventoryService.getAllProducts().size() + 1;

        inventoryService.addProduct(PRODUCT_NAME, PRODUCT_PRICE, 15, 1, 15, productParts);
        int actualListSize = inventoryService.getAllProducts().size();

        assertEquals(expectedListSize, actualListSize);
    }

    @Test
    @DisplayName("MinEqualTo0")
    void testAddProductMin0() {
        int expectedListSize = inventoryService.getAllProducts().size() + 1;

        inventoryService.addProduct(PRODUCT_NAME, PRODUCT_PRICE, 1, 0, 15, productParts);
        int actualListSize = inventoryService.getAllProducts().size();

        assertEquals(expectedListSize, actualListSize);
    }

    @Test
    @DisplayName("MinEqualToMax")
    void testAddProductMinEqualMax() {
        int expectedListSize = inventoryService.getAllProducts().size() + 1;

        inventoryService.addProduct(PRODUCT_NAME, PRODUCT_PRICE, 15, 15, 15, productParts);
        int actualListSize = inventoryService.getAllProducts().size();

        assertEquals(expectedListSize, actualListSize);
    }

    @Test
    @DisplayName("MinBiggerThanMax")
    void testAddProductMinBiggerThanMax() {
        String expectedErrorMessage = "The Min value must be less than the Max value. Inventory level is lower than minimum value. ";

        Throwable exception = assertThrows(RuntimeException.class,
                () -> inventoryService.addProduct(PRODUCT_NAME, PRODUCT_PRICE, 15, 16, 15, productParts));

        assertEquals(expectedErrorMessage, exception.getMessage());
    }
}
