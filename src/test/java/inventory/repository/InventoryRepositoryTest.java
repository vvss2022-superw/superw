package inventory.repository;

import inventory.model.InhousePart;
import inventory.model.Inventory;
import inventory.model.Part;
import inventory.model.Product;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class InventoryRepositoryTest {

    @InjectMocks
    private InventoryRepository repository;

    @Mock
    private Inventory inventory;

    private static ObservableList<Part> productParts;

    private static ObservableList<Product> products;

    @BeforeAll
    static void setUp() {
        productParts = FXCollections.observableArrayList();
        productParts.add(new InhousePart(1, "lemn", 1, 5, 3, 10, 1));

        products = FXCollections.observableArrayList();
        Product product = new Product(1, "dulap", 10.0, 10, 5, 15, productParts);
        products.add(product);
    }

    @Test
    void getAllProductsTest() {
        when(inventory.getProducts()).thenReturn(products);

        ObservableList<Product> result = repository.getAllProducts();

        assertEquals(products, result);
        verify(inventory).getProducts();
    }

    @Test
    void getAllPartsTest() {
        when(inventory.getAllParts()).thenReturn(productParts);

        ObservableList<Part> result = repository.getAllParts();

        assertEquals(productParts, result);
        verify(inventory).getAllParts();
    }
}
