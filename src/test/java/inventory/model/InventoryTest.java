package inventory.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class InventoryTest {

    private static Inventory inventory = new Inventory();
    private static Product product;
    private static final Product DEFAULT_PRODUCT = new Product(0, null, 0.0, 0, 0, 0, null);

    @BeforeAll
    static void setUp() {
        ObservableList<Part> productParts = FXCollections.observableArrayList();
        productParts.add(new InhousePart(1, "lemn", 100, 5, 3, 10, 1));

        product = new Product(1, "dulap", 10.0, 10, 5, 15, productParts);
        inventory.addProduct(product);
    }

    @Test
    void testLookupProductEmptySearchItem() {
        String searchItem = "";

        Product result = inventory.lookupProduct(searchItem);

        assertEquals(null, result);
    }

    @Test
    void testLookupProductByName() {
        String searchItem = "a";

        Product result = inventory.lookupProduct(searchItem);

        assertEquals(product, result);
    }

    @Test
    void testLookupProductById() {
        String searchItem = "1";

        Product result = inventory.lookupProduct(searchItem);

        assertEquals(product, result);
    }

    @Test
    void testLookupProductNotFound() {
        String searchItem = "30";

        Product result = inventory.lookupProduct(searchItem);

        assertEquals(DEFAULT_PRODUCT.getName(), result.getName());
        assertEquals(DEFAULT_PRODUCT.getProductId(), result.getProductId());
        assertEquals(DEFAULT_PRODUCT.getPrice(), result.getPrice());
    }

    @Test
    void testLookUpProductEmptyList() {
        String searchItem = "30";

        Product result = inventory.lookupProduct(searchItem);

        assertEquals(DEFAULT_PRODUCT.getName(), result.getName());
        assertEquals(DEFAULT_PRODUCT.getProductId(), result.getProductId());
        assertEquals(DEFAULT_PRODUCT.getPrice(), result.getPrice());
    }
}
