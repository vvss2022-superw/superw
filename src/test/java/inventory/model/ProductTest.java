package inventory.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ProductTest {
    private static final String NAME = "dulap";
    private static final double PRICE = 10.0;

    private static Product product;

    @BeforeAll
    static void setUp() {
        ObservableList<Part> productParts = FXCollections.observableArrayList();
        productParts.add(new InhousePart(1, "lemn", 100, 5, 3, 10, 1));

        product = new Product(1, NAME, PRICE, 10, 5, 15, productParts);
    }

    @Test
    void getNameTest() {

        String result = product.getName();

        assertEquals(NAME, result);
    }

    @Test
    void getPriceTest() {

        double result = product.getPrice();

        assertEquals(PRICE, result);
    }
}
